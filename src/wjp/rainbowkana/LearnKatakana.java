package wjp.rainbowkana;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Aktywnosc pozwalajaca na nauke tablicy hiragany
 * @author Sylwia Mikolajczuk
 */
public class LearnKatakana extends Activity {
	
	Context context = this;
	
	/**
	 * Inicjalizacja aktywnosci (automatycznie wygenerowana)
	 * @param savedInstanceState Zapamietane dane z poprzedniej inicjalizacji (je�li aktywnosc jest reinicjalizowana)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
				
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_learn_katakana);
		// Show the Up button in the action bar.
		setupActionBar();
		
	}

	/**
	 * Utworzenie paska menu
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}
	
	/**
	 * Dodanie element�w do menu
	 * @param menu Menu na pasku
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.practice_hiragana, menu);
		return true;
	}

	/**
	 * Reakcja na wybranie akcji z menu
	 * @param item Wybrany przycisk
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Pokaz transkrypcje lacinska znaku i pozwol na zaznaczenie buttona jako nauczony
	 * @param view Klikniety obiekt
	 */
	public void show(View view){
		
		//Poka� transkrypcj�
		Button pressedButton = (Button)view;
		String kana = (String) pressedButton.getText();
		String showTrans = Ch.katToRom(kana);
		
		Toast transcription = Toast.makeText(context, showTrans, Toast.LENGTH_SHORT);
		LinearLayout toastLayout = new LinearLayout(context);
		TextView trans = new TextView(context);
		
		trans.setText(showTrans);
		trans.setTextSize(80);
		trans.setGravity(Gravity.CENTER);
		trans.setBackgroundResource(R.drawable.learn_toast);
		
		toastLayout.addView(trans);
		transcription.setView(toastLayout);
		transcription.setGravity(Gravity.CENTER, 0, 0);
		transcription.show();
		
		//Dodaj mo�liwo�� zaznaczenia jako nauczone
		pressedButton.setOnLongClickListener(new OnLongClickListener(){
			@Override
			public boolean onLongClick(View view){
				Button pressedButton = (Button)view;
				
				if(pressedButton.getTextColors().getDefaultColor()==Color.parseColor("#775921")){
					pressedButton.setBackgroundResource(R.drawable.button_learned);
					pressedButton.setTextColor(Color.parseColor("#27986e"));
				}
				else{
					pressedButton.setBackgroundResource(R.drawable.learn_button);
					pressedButton.setTextColor(Color.parseColor("#775921"));
				}
				return true;
			}
		});
	}
}