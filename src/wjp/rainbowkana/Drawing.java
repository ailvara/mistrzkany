package wjp.rainbowkana;

import java.util.Random;

/**  
 * Klasa odpowiedzialna za losowanie slow i znakow 
 * @author Sylwia Mikolajczuk
 */
public class Drawing {
	
	/** Ostatnio wylosowane slowo pisane w hiraganie */
	private static int lastHiragana = -1;
	
	/** Ostatnio wylosowane slowo pisane w katakanie */
	private static int lastKatakana = -1;
	
	/** 
	 * Losowanie slowa do cwiczenia na podstawie stopnia opanowania, z wylaczeniem ostatnio cwiczonego slowa
	 * @param set Zbior slow do losowania
	 * @param prof Tablica wartosci stopnia opanowania
	 * @param syllabary Katakana czy hiragana
	 * @return Indeks (polozenie w tablicy) wylosowanego slowa
	 */
	//Funkcja zmodyfikowana po nagraniu filmiku, ale (mam nadziej�) bez skutk�w dla prezentacji.
	public static int drawWord(String[][] set, int prof[], int syllabary){
		
		int last;
		if(syllabary==1) last = lastHiragana;
		else last = lastKatakana;
		
		boolean isThere=false;
		int wanted;
		
		do {
			//Losowanie z najwi�kszym prawdopodobie�stwem wylosowania s�abo opanowanego s�owa
			int rand = new Random().nextInt(20);
			if(rand<1) wanted=5;
			else if(rand<3) wanted=4;
			else if(rand<6) wanted=3;
			else if(rand<10) wanted=2;
			else if(rand<15) wanted=1;
			else wanted=0;
			
			//Sprawdzenie, czy istnieje s�owo o danym poziomie
			for(int a : prof){
				if(a==wanted) isThere=true;
			}
			
		} while(isThere==false);
			
		int r;
		
		//Wylosowanie s�owa o podanym poziomie, innego ni� w poprzednim �wiczeniu
		do {
			r = new Random().nextInt(set.length);
		} while(prof[r]!=wanted || r==last);
		
		if(syllabary==1) lastHiragana = r;
		else lastKatakana=r;
		return r;
	}
	
	/*
	 * Losowanie slowa do cwiczenia katakany na podstawie stopnia opanowania, z wylaczeniem ostatnio cwiczonego slowa
	 * @return Indeks (polozenie w tablicy) wylosowanego slowa
	 
	public static int drawKatakanaWord(){
		
		boolean isThere=false;
		int wanted;
		
		do {
			//Losowanie z najwi�kszym prawdopodobie�stwem wylosowania s�abo opanowanego s�owa
			int rand = new Random().nextInt(20);
			if(rand<1) wanted=5;
			else if(rand<3) wanted=4;
			else if(rand<6) wanted=3;
			else if(rand<10) wanted=2;
			else if(rand<15) wanted=1;
			else wanted=0;
			
			//Sprawdzenie, czy istnieje s�owo o danym poziomie
			for(int a : Base.kProf){
				if(a==wanted) isThere=true;
			}
						
		} while(isThere==false);
			
		int r;
		
		
		//Wylosowanie s�owa o podanym poziomie, innego ni� w poprzednim �wiczeniu
		do {
			r = new Random().nextInt(Base.katSet.length);
		} while(Base.kProf[r]!=wanted || r==lastKatakana);
		
		lastKatakana = r;
		return r;
	}*/
	
	/**
	 * Losowe rozmieszczenie sylab slowa i losowe dopelnienie niepoprawnymi znakami
	 * @param word Slowo, ktorego sylaby musza znalezc sie w tablicy
	 * @param buttonCount Liczba sylab (buttonow)
	 * @param syllabary Uzywany sylabariusz
	 * @return Tablica sylab do utworzenia buttonow
	 */
	public static char[] randomSyllables(Word word, int buttonCount, int syllabary){
		
		//Tablica znak�w, na podstawie kt�rej zostan� dynamicznie dodane buttony
		char toChoose[] = new char[buttonCount];
	    
		
	    //Dodanie to tablicy znak�w ze s�owa
	    Random rnd = new Random();
	    int index;
	    
	    for(int g=0; g<word.kana.length(); g++){
	    	index = rnd.nextInt(buttonCount);
	    	if(toChoose[index]=='\u0000') toChoose[index] = word.kana.charAt(g);
	    	else g--;
	    }
	    
	    
	    //Uzupe�nienie losowymi niepoprawnymi znakami pustych element�w tablicy
	    char temp;
	    boolean is=false;
	    
	    for(int g=0; g<buttonCount; g++){
	    	
	    	if(toChoose[g]=='\u0000') {
	    		
	    		do {
	    			if(syllabary==1) temp=(Ch.characters[new Random().nextInt(Ch.characters.length)].hir);
	    			else temp=(Ch.characters[new Random().nextInt(Ch.characters.length)].kat);
	    			is=false;
	    			
	    			//Zapobieganie powtarzaniu znak�w
	    			for(char item : toChoose){
	    				if(temp==item) is=true;
	    			}
	    			
	    			if(!is) toChoose[g]=temp;
	    			
	    		} while(is);
	    	}
	    }
		
		return toChoose;
	}
	
}