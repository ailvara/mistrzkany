package wjp.rainbowkana;

/** Klasa tymczasowo przechowujaca dane programu */
public class Base {
	
	/** Przykladowy zbior slow i ciekawostek do cwiczenia hiragany */
	static String[][] hirSet = {
		
		//Owoce i warzywa
		{"jab�ko","ri-n-go", "W mitologii greckiej jab�ka by�y symbolem zar�wno Demeter, Hery jak i Afrodyty."},
		{"truskawka","i-chi-go", ""},
		{"marchewka","ni-n-ji-n",""},
		{"kukurydza","to-u-mo-ro-ko-shi","Na kolbach kukurydzy ro�nie pewien jadalny gatunek grzyba."},
		{"cebula","ta-ma-ne-gi","Jedzenie podgotowanej cebuli pozytywnie wp�ywa na pami�� i kojarzenie fakt�w."},
		{"gruszka","na-shi",""},
		{"dynia","ka-bo-#cha","Najci�sz� dyni� �wiata wychodowano w 2012 roku. Wa�y�a ponad ton�."},
		{"ziemniak","#ja-ga-i-mo",""},
		
		//Przyroda i ekologia
		{"drzewo","ki",""},
		{"kwiat","ha-na","W XVII wieku w Holandii cebulki tulipan�w przewy�sza�y warto�ci� z�oto."},
		{"rzeka","ka-wa","Najd�u�sz� rzek� �wiata jest Amazonka."},
		{"natura","shi-ze-n",""},
		{"morze","u-mi",""},
		{"las","ha-ya-shi",""},
		{"g�ra","ya-ma","Najwy�sz� znan� g�r� jest Olympus Mons na Marsie. Ma ok. 26 000 metr�w"},
		{"jezioro","mi-zo-u-mi",""},
		{"kwitn�ca wi�nia","sa-ku-ra",""}
		
	};
	
	/** Tablica przechowujaca stopien opanowania slow pisanych hiragana*/
	public static int hProf[] = {
		4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, //Warto�ci testowe
	};
	
	/** Przykladowy zbior slow i ciekawostek do cwiczenia katakany */
	static String[][] katSet = {
		{"ananas","pa-i-na-%p%-pu-ru",""},
		{"kapusta","#kya-be-tsu",""},
		{"banan", "ba-na-na",""},
		{"cytryna","re-mo-n",""},
		{"pomara�cza","o-re-n-ji",""},
		{"pomidor","to-ma-to",""},
	};
	
	/** Tablica przechowujaca stopien opanowania slow pisanych katakana*/
	public static int kProf[] = {
		1, 1, 5, 5, 5, 5 //Warto�ci testowe
	};
	
}