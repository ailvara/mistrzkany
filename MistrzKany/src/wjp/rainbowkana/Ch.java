package wjp.rainbowkana;

/**
 * Klasa zajmujaca sie przechodzeniem pomiedzy roznymi systemami.
 * Obiekt klasy zawiera informacje o odpowiadajacych sobie znakach w roznych systemach
 * Metody klasy pozwalaja na zlozona transkrypcje zapisu.
 */
public class Ch {
	
		/** Transkrypcja lacinska znaku*/
		public String rom;
		/** Zapis znaku w hiraganie*/
		public char hir;
		/** Zapis znaku w katakanie*/
		public char kat;
		
		/**
		 * Utworzenie obiektu-znaku
		 * @param rom Transkrypcja lacinska znaku
		 * @param hir Zapis znaku w hiraganie
		 * @param kat Zapis znaku w katakanie
		 */
		public Ch(String rom, char hir, char kat){
			this.rom=rom;
			this.hir=hir;
			this.kat=kat;
		}

	/** Tablica relacji transkrypcja lacinska-hiragana-katakana */
	public static Ch characters[] = {
			
			new Ch("a", '\u3042', '\u30A2'),
			new Ch("i", '\u3044', '\u30A4'),
			new Ch("u", '\u3046', '\u30A6'),
			new Ch("e", '\u3048', '\u30A8'),
			new Ch("o", '\u304A', '\u30AA'),
			
			new Ch("ka", '\u304B', '\u30AB'),
			new Ch("ki", '\u304D', '\u30AD'),
			new Ch("ku", '\u304F', '\u30AF'),
			new Ch("ke", '\u3051', '\u30B1'),
			new Ch("ko", '\u3053', '\u30B3'),
			
			new Ch("ga", '\u304C', '\u30AC'),
			new Ch("gi", '\u304E', '\u30AE'),
			new Ch("gu", '\u3050', '\u30B0'),
			new Ch("ge", '\u3052', '\u30B2'),
			new Ch("go", '\u3054', '\u30B4'),
			
			new Ch("sa", '\u3055', '\u30B5'),
			new Ch("shi", '\u3057', '\u30B7'),
			new Ch("su", '\u3059', '\u30B9'),
			new Ch("se", '\u305B', '\u30BB'),
			new Ch("so", '\u305D', '\u30BD'),
			
			new Ch("za", '\u3056', '\u30B6'),
			new Ch("ji", '\u3058', '\u30B8'),
			new Ch("zu", '\u305A', '\u30BA'),
			new Ch("ze", '\u305C', '\u30BC'),
			new Ch("zo", '\u305E', '\u30BE'),
			
			new Ch("ta", '\u305F', '\u30BF'),
			new Ch("chi", '\u3061', '\u30C1'),
			new Ch("tsu", '\u3064', '\u30C4'),
			new Ch("te", '\u3066', '\u30C6'),
			new Ch("to", '\u3068', '\u30C8'),
			
			new Ch("da", '\u3060', '\u30C0'),
			new Ch("du", '\u3065', '\u30C2'),
			new Ch("de", '\u3067', '\u30C5'),
			new Ch("do", '\u3069', '\u30C7'),
			
			new Ch("na", '\u306A', '\u30CA'),
			new Ch("ni", '\u306B', '\u30CB'),
			new Ch("nu", '\u306C', '\u30CC'),
			new Ch("ne", '\u306D', '\u30CD'),
			new Ch("no", '\u306E', '\u30CE'),
			
			new Ch("ha", '\u306F', '\u30CF'),
			new Ch("hi", '\u3072', '\u30D2'),
			new Ch("fu", '\u3075', '\u30D5'),
			new Ch("he", '\u3078', '\u30D8'),
			new Ch("ho", '\u307B', '\u30DB'),
			
			new Ch("ba", '\u3070', '\u30D0'),
			new Ch("bi", '\u3073', '\u30D3'),
			new Ch("bu", '\u3076', '\u30D6'),
			new Ch("be", '\u3079', '\u30D9'),
			new Ch("bo", '\u307C', '\u30DC'),
			
			new Ch("pa", '\u3071', '\u30D1'),
			new Ch("pi", '\u3074', '\u30D4'),
			new Ch("pu", '\u3077', '\u30D7'),
			new Ch("pe", '\u307A', '\u30DA'),
			new Ch("po", '\u307D', '\u30DD'),
			
			new Ch("ma", '\u307E', '\u30DE'),
			new Ch("mi", '\u307F', '\u30DF'),
			new Ch("mu", '\u3080', '\u30E0'),
			new Ch("me", '\u3081', '\u30E1'),
			new Ch("mo", '\u3082', '\u30E2'),
			
			new Ch("ya", '\u3084', '\u30E4'),
			new Ch("yu", '\u3086', '\u30E6'),
			new Ch("yo", '\u3088', '\u30E8'),
			
			new Ch("ra", '\u3089', '\u30E9'),
			new Ch("ri", '\u308A', '\u30EA'),
			new Ch("ru", '\u308B', '\u30EB'),
			new Ch("re", '\u308C', '\u30EC'),
			new Ch("ro", '\u308D', '\u30ED'),
			
			new Ch("wa", '\u308F', '\u30EF'),
			new Ch("wo", '\u3092', '\u30F2'),
			new Ch("n", '\u3093', '\u30F3'),
			new Ch("smallTsu", '\u3063', '\u30C3')
			
	};
	
	/**
	 * Funkcja przerabiajaca ciag znakow w tranksrypcji lacinskiej na hiragane
	 * @param word Slowo do przetlumaczenia
	 * @return Zapis w hiraganie
	 */
	public static String toHir(String word){
		
		String[] syllab = word.split("-");
		String h="";
				
		for(String syl : syllab){
			
			String searched = searchedSyllabe(syl);
			
			for(Ch s : characters){
				if(searched.equals(s.rom)) h+=s.hir;
			}
			
			if(syl.charAt(0)=='#'){
				switch(syl.charAt(syl.length()-1)){
				case 'a': h+="\u3083"; break;
				case 'u': h+="\u3085"; break;
				case 'o': h+="\u3087"; break;
				}
			}
		}
		return h;
	}
		
	/**
	 * Funkcja przerabiajaca ciag znakow w tranksrypcji lacinskiej na katakane
	 * @param word Slowo do przetlumaczenia
	 * @return Zapis w katakanie
	 */
	public static String toKat(String word){
		
		String[] syllab = word.split("-");
		String k="";
		
		for(String syl : syllab){
			
			String searched = searchedSyllabe(syl);
			
			for(Ch s : characters){
				if(searched.equals(s.rom)) k+=s.kat;
			}
			
			//Sylaby podw�jne
			if(syl.charAt(0)=='#'){
				switch(syl.charAt(syl.length()-1)){
				case 'a': k+="\u30E3"; break;
				case 'u': k+="\u30E5"; break;
				case 'o': k+="\u30E7"; break;
				}
			}
		}
		return k;
	}
	
	/**
	 * Funkcja ustalajaca, jakiej sylaby nalezy szukac w tablicy (uwzglednienie podwojen spolglosek, sylab podwojnych)
	 * @param syl Sylaba do zdekodowania
	 * @return Sylaba w formie mozliwej do odszukania w tablicy
	 */
	private static String searchedSyllabe(String syl){
		
		String searched=null;
		
		//Podwojone sp�g�oski
		if(syl.charAt(0)=='%') searched="smallTsu";
		
		//Sylaby podw�jne
		else if(syl.charAt(0)=='#') {
			if(syl.charAt(1)=='s') searched="shi";
			else if(syl.charAt(1)=='t' || syl.charAt(1)=='c') searched="chi";
			else searched=""+syl.charAt(1)+'i';
		}
		
		else searched=syl;
		return searched;
		
	}
	
	/**
	 * Funkcja przejscia z hiragany na transkrypcje lacinska
	 * @param syllable Sylaba do przetlumaczenia
	 * @return Zapis w roomaji
	 */
	public static String hirToRom(String syllable){
		
		String roomaji=null;
		for(Ch s : characters){
			if(syllable.charAt(0)==s.hir) roomaji=s.rom;
		}
		return roomaji;
	}
	
	/**
	 * Funkcja przejscia z katakany na transkrypcje lacinska.
	 * @param syllable Sylaba do przetlumaczenia
	 * @return Zapis w roomaji
	 */
	public static String katToRom(String syllable){
		
		String roomaji=null;
		for(Ch s : characters){
			if(syllable.charAt(0)==s.kat) roomaji=s.rom;
		}
		return roomaji;
	}

}