package wjp.rainbowkana;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

/**
 * Aktywnosc cwiczenia hiragany
 * @author Sylwia Mikolajczuk
 */
public class PracticeHiragana extends Activity {
	
	/** Liczba punktow zdobytych we wszystkich cwiczeniach */
	public static int points=0;
	
	/** Kontekst aktywnosci */
	Context context = this;
	
	/** Liczba popelnionych bledow */
	int fails=0;
	
	/** Liczba punktow do zdobycia za cwiczenie */
	int maxPoints = 5;
	
	/** Liczba buttonow (znakow) */
	int buttonCount=10;
	
	/** Czy cwiczenie zostalo juz ukonczone */
	boolean end=false;
	
	/** Czy slowo zostalo juz wczesniej zaliczone na 5 gwiazdek */
	boolean passed=false;

	/**
	 * Inicjalizacja aktywnosci (automatycznie wygenerowana)
	 * @param savedInstanceState Zapamietane dane z poprzedniej inicjalizacji (je�li aktywno�� jest reinicjalizowana)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
				
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_practice_hiragana);
		// Show the Up button in the action bar.
		setupActionBar();
		
		//Utw�rz obiekt i napisy
		final int ind = Drawing.drawHiraganaWord();
		final Word word = new Word(Base.hirSet[ind][0], Base.hirSet[ind][1], 0, Base.hirSet[ind][2]);
		char[] toChoose = Drawing.randomSyllables(word, buttonCount, 1);
		
		if(Base.hProf[ind]==5) passed=true;
		
		TextView roomaji = (TextView) findViewById(R.id.roomaji);
	    roomaji.setText(word.roomaji);
	    
	    TextView latin = (TextView) findViewById(R.id.latin);
	    latin.setText(word.translation);
	    
	    TextView h = (TextView) findViewById(R.id.kana);
	    h.setText("");
	    
	    RatingBar p = (RatingBar)findViewById(R.id.proficiency);
	    p.setRating(Base.hProf[ind]);
	    
	    final TextView showPoints = (TextView) findViewById(R.id.points);
	    showPoints.setText(""+points);
	    
	    
	    //Utw�rz buttony
	    int j=0;
	    LinearLayout l1 = (LinearLayout)findViewById(R.id.buttons1);
    	LinearLayout l2 = (LinearLayout)findViewById(R.id.buttons2);
    	    	
	    for(char syl:toChoose){
	    	
	    	Button b = new Button(this);
	    	b.setText(Character.toString(syl));
	    	b.setBackgroundResource(R.drawable.practice_button);
	    	b.setTextColor(Color.WHITE);
	    	b.setTextSize(40);
	    	b.setHeight(100);
        	b.setWidth(100);
        	
	    	//Przyci�ni�cie przycisku
	    	b.setOnClickListener(new View.OnClickListener() {
	    		
	             public void onClick(View view) {
	            	 
	            	Button b = (Button)view;
	         	    String buttonText = b.getText().toString();
	         	    TextView text = (TextView) findViewById(R.id.kana);
	         	    String prevText = text.getText().toString();
	         	    
	         	    int s = prevText.length();
	         	    
	         	    if(end==true);
	         	    
	         	    else if(word.kana.charAt(s)==buttonText.charAt(0)){
	         	    	text.setText(prevText+buttonText);
	         	    	b.setBackgroundResource(R.drawable.good);
	         	    	b.setClickable(false);
	         	    }
	         	    
	         	    else {
	         	    	fails++;
	         	    	Button f;
	         	    	if(fails==1) f = (Button)findViewById(R.id.fail1);
	         	    	else if(fails==2) f = (Button)findViewById(R.id.fail2);
	         	    	else f = (Button)findViewById(R.id.fail3);
	         	    	f.setBackgroundResource(R.drawable.fail);
	         	    	maxPoints--;
	         	    }
	         	    
	         	    if(text.getText().equals(word.kana) && end==false){
	         	    	end=true;
	         	    	Base.hProf[ind]++;
	         	    	if(Base.hProf[ind]>=5 && passed==false) {
	         	    		Base.hProf[ind]=5;
	         	    		if(word.isTrivia==true) word.showTrivia(context);
	         	    	}
	         	    	text.setTextColor(Color.parseColor("#27986e"));
	         	    	points+=maxPoints;
	         	    	showPoints.setText(""+points);
	         	    	Button next = (Button)findViewById(R.id.next);
	         	    	next.setClickable(true);
	         	    	next.setBackgroundResource(R.drawable.next_button_active);
	         	    }
	         	    
	         	    if(fails==4 && end==false){
	         	    	maxPoints=0;
	         	    	end=true;
	         	    	text.setTextColor(Color.RED);
	         	    	text.setText(word.kana);
	         	    	Button next = (Button)findViewById(R.id.next);
	         	    	next.setClickable(true);
	         	    	next.setBackgroundResource(R.drawable.next_button_active);
	         	    }
	             }
	             
	    	});//Koniec listenera
	    	
	    	j++;
	    	if(j<6) l1.addView(b);
	    	else l2.addView(b);
	    	
	    }//Koniec p�tli
		
	}

	/**
	 * Utworzenie paska menu
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	/**
	 * Dodanie element�w do menu
	 * @param menu Menu na pasku
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.practice_hiragana, menu);
		return true;
	}

	/**
	 * Reakcja na wybranie akcji z menu
	 * @param item Wybrany przycisk
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			//points=0; wyzerowanie punkt�w
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Przejscie do kolejnego cwiczenia
	 * @param view Klikniety obiekt
	 */
	public void next(View view) {
		Intent intent = new Intent(this, PracticeHiragana.class);
		startActivity(intent);
	}
	
}