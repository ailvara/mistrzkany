
package wjp.rainbowkana;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

/**
 * Okno menu
 * @author Sylwia Mikolajczuk
 */
public class Intro extends Activity {

	/**
	 * Inicjalizacja aktywnosci (automatycznie wygenerowana)
	 * @param savedInstanceState Zapamietane dane z poprzedniej inicjalizacji (je�li aktywnosc jest reinicjalizowana)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_intro);
	}

	/**
	 * Dodanie element�w do menu
	 * @param menu Menu na pasku
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.intro, menu);
		return true;
	}
	
	/**
	 * W�acz tablic� hiragany
	 * @param view Klikniety przycisk
	 */
	public void launchLearnHiragana(View view){
		Intent intent = new Intent(this, LearnHiragana.class);
		startActivity(intent);
	}
	
	/**
	 * W�acz �wiczenie hiragany
	 * @param view Klikniety przycisk
	 */
	public void launchPracticeHiragana(View view){
		Intent intent = new Intent(this, PracticeHiragana.class);
		startActivity(intent);
	}
	
	/**
	 * W�acz tablic� katakany
	 * @param view Klikniety przycisk
	 */
	public void launchLearnKatakana(View view){
		Intent intent = new Intent(this, LearnKatakana.class);
		startActivity(intent);
	}
	
	/**
	 * W�acz �wiczenie katakany
	 * @param view Klikniety przycisk
	 */
	public void launchPracticeKatakana(View view){
		Intent intent = new Intent(this, PracticeKatakana.class);
		startActivity(intent);
	}

}