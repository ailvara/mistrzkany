package wjp.rainbowkana;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/** 
 * Klasa modelujaca slowo u�ywane w �wiczeniu
 * @author Sylwia Mikolajczuk
 */
public class Word {
	
	/** Zapis zawierajacy informacje o podwojnych sylabach */
	private String value;
	
	/** Znaczenie slowa w jezyku polskim */
	public String translation;
	
	/** Zapis lacinski */
	public String roomaji;
	
	/** Zapis w hiraganie lub katakanie */
	public String kana;
	
	/** Tresc ciekawostki odblokowanej po wycwiczeniu slowa */
	private String trivia;
	
	/** Okresla, czy istnieje ciekawostka przypisana do slowa */
	public boolean isTrivia;
	
	/** Tablica sylab slowa */
	public String[] syllables = new String[0];

	/**
	 * Konstruktor klasy Word
	 * @param translation Znaczenie slowa w jezyku polskim
	 * @param value Zapis zawieraj�cy informacje o podwojnych sylabach
	 * @param syllabary Odpowiadaj�cy sylabariusz
	 * @param trivia  Tresc ciekawostki odblokowanej po wycwiczeniu slowa
	 */
	public Word (String translation, String value, int syllabary, String trivia){
		
		this.value = value;
		this.translation = translation;
		this.trivia = trivia;
		
		roomaji = roomaji();
		syllables = value.split("-");
		
		if(syllabary==0) kana = Ch.toHir(value);
		if(syllabary==1) kana = Ch.toKat(value);
		
		if(trivia=="") isTrivia=false;
		else isTrivia=true;
		
	}

	/** 
	 * Usuniecie znacznikow z transkrypcji roomaji
	 * @return rom Zwraca gotowy lancuch
	 */
	private String roomaji(){
		
		String rom = value.replace("%-","");
		rom = rom.replace("-", " ");
		rom = rom.replace("#","");
		rom = rom.replace("%","");
		return rom;
		
	}
	
	/** 
	 * Wyswietlenie ciekawostki
	 * @param context Kontekst do utworzenia okna przeslany z activity
	 */
	public void showTrivia(Context context){
		
		final Dialog triviaDialog = new Dialog(context);
		triviaDialog.setContentView(R.layout.dialog);
		triviaDialog.setTitle(R.string.trivia_unlocked);
		
		Button ok = (Button)triviaDialog.findViewById(R.id.ok);
		ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View arg0) {
				triviaDialog.dismiss();
			}
		});
		
		TextView content = (TextView)triviaDialog.findViewById(R.id.content);
		TextView bold = (TextView)triviaDialog.findViewById(R.id.bold);
		
		bold.setText(R.string.trivia_title);
		content.setText(trivia);
		
		triviaDialog.show();
		
	}
		
}